<?php

namespace Model_Setup\Model;

use Illuminate\Support\ServiceProvider;

class ModelServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__.'Models/YourModel.php' => app_path('Models/YourModel.php'),
        ], 'models');
    }

    public function register()
    {
        //
    }
}
